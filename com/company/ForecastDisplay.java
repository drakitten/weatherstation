package com.company;

public class ForecastDisplay implements Observer, DisplayElement {

    private float lastPressure;
    private float currPressure = 29;
    private Subject weatherData;

    public ForecastDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.addObserver(this);
    }

    @Override
    public void display() {
        System.out.print("Forecast: ");
        if (currPressure > lastPressure) {
            System.out.println("Improving weather on the way!");
        } else if (currPressure < lastPressure) {
            System.out.println("Watch out for cooler, rainy weather");
        } else System.out.println("More of the same");
    }

    @Override
    public void update(float temperature, float humidity, float pressure) {
        lastPressure = currPressure;
        currPressure = pressure;
        display();
    }
}
