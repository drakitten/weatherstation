package com.company;

public class StatisticsDisplay implements Observer, DisplayElement {

    private float maxTemp = 0f;
    private float minTemp = 200;
    private float sumTemp = 0f;
    private int num = 0;
    private Subject weatherData;

    public StatisticsDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.addObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + sumTemp/num + "/" + maxTemp + "/" +minTemp);
    }

    @Override
    public void update(float temperature, float humidity, float pressure) {
        if (maxTemp < temperature) {
            maxTemp = temperature;
        }
        if (minTemp > temperature) {
            minTemp = temperature;
        }
        sumTemp += temperature;
        num++;
        display();
    }
}
